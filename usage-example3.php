<?php

use Example3\RangedStrengthCalculator;
use Example3\Unit\Archer;
use Example3\Unit\LongBowArcher;
use Example3\Unit\Swordsman;
use Example3\Weather;

require_once __DIR__ . '/vendor/autoload.php';

$calculator = new RangedStrengthCalculator();

echo $calculator->calculateStrength(new Archer(), 10, Weather::NICE);
echo $calculator->calculateStrength(new LongBowArcher(), 10, Weather::NICE);
echo $calculator->calculateStrength(new Swordsman(), 10, Weather::NICE);
