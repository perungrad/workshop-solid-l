<?php

use Example4\Article;
use Example4\ArticleMessage;

require_once __DIR__ . '/vendor/autoload.php';

$article = new Article();
$article->setId(42);
$article->setText('Lorem Ipsum');

$message = new ArticleMessage();
$message->setText('Star Trek Ipsum');

echo $article->addMessage($message);
