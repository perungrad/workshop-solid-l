<?php

namespace Example2;

use Example2\BuildingInterface;

interface BuildingsProviderInterface
{
    /**
     * @return BuildingInterface[]
     */
    public function getBuildings();
}

