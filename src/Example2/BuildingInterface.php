<?php

namespace Example2;

interface BuildingInterface
{
    public function getName(): string;
}

