<?php

namespace Example2;

use Example2\Building;
use Example2\BuildingInterface;

class BuildingsProvider implements BuildingsProviderInterface
{
    /**
     * @return BuildingInterface[]
     */
    public function getBuildings()
    {
        $buildings = [];

        $buildings[] = new Building('barracks');
        $buildings[] = new Building('storehouse');

        return $buildings;
    }
}

