<?php

namespace Example2;

use Example2\BuildingInterface;
use Example2\BuildingCollection;
use Example2\Building;

class PremiumBuildingsProvider implements BuildingsProviderInterface
{
    /**
     * @return BuildingInterface[]
     */
    public function getBuildings()
    {
        $buildings = new BuildingCollection();

        $buildings->addBuilding(new Building('market'));
        $buildings->addBuilding(new Building('castle'));

        return $buildings;
    }
}

