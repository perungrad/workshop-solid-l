<?php

namespace Example2;

use Example2\BuildingInterface;

class BuildingCollection implements \Iterator
{
    private $buildings = [];

    private int $position = 0;

    public function addBuilding(BuildingInterface $building): self
    {
        $this->buildings[] = $building;

        return $this;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->buildings[$this->position];
    }

    /**
     * @return integer
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return void
     */
    public function next()
    {
        $this->position += 1;
    }

    /**
     * @return void
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return boolean
     */
    public function valid()
    {
        return isset($this->buildings[$this->position]);
    }
}

