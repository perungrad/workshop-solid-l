<?php

namespace Example4;

use Example4\MessageInterface;

class Message implements MessageInterface
{
    private int $id;

    private int $userId;

    private string $text;

    private \DateTime $addedAt;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setAddedAt(\DateTime $addedAt): self
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    public function getAddedAt(): \DateTime
    {
        return $this->addedAt;
    }
}
