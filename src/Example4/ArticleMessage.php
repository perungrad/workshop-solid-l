<?php

namespace Example4;

use Example4\Message;

class ArticleMessage extends Message
{
    private int $articleId;

    public function setArticleId(int $articleId): self
    {
        $this->articleId = $articleId;

        return $this;
    }

    public function getArticleId(): int
    {
        return $this->articleId;
    }
}
