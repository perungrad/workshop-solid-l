<?php

namespace Example4;

interface MessageInterface
{
    public function setId(int $id): self;

    public function getId(): int;

    public function setText(string $text): self;

    public function getText(): string;

    public function setUserId(int $userId): self;

    public function getUserId(): int;

    public function setAddedAt(\DateTime $addedAt): self;

    public function getAddedAt(): \DateTime;
}
