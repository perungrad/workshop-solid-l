<?php

namespace Example4;

use Example4\MessageInterface;

class Article
{
    private int $id;

    private string $text;

    /** @var array<MessageInterface> */
    private array $messages;

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function addMessage(MessageInterface $message): self
    {
        $message->setArticleId($this->id);

        $this->messages[] = $message;

        return $this;
    }
}
