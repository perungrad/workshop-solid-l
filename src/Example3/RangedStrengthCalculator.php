<?php

namespace Example3;

use Example3\RangedStrengthCalculatorInterface;
use Example3\Unit\Archer;
use Example3\Unit\LongBowArcher;
use Example3\Unit\UnitInterface;

class RangedStrengthCalculator implements RangedStrengthCalculatorInterface
{
    public function calculateStrength(UnitInterface $unit, int $howMany, int $weatherType): int
    {
        if (!($unit instanceof Archer) && !($unit instanceof LongBowArcher)) {
            throw new \Exception('Invalid unit type');
        }

        $strength = $unit->getAttackStrength() * $howMany;

        if ($this->weatherType === Weather::RAIN) {
            $strength = floor($strength * 0.25);
        }

        return $strength;
    }
}
