<?php

namespace Example3\Unit;

interface UnitInterface
{
    public function getAttackStrength(): int;
}
