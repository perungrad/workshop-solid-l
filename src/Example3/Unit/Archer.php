<?php

namespace Example3\Unit;

use Example3\Unit\UnitInterface;

class Archer implements UnitInterface
{
    public function getAttackStrength(): int
    {
        return 5;
    }
}
