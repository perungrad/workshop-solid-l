<?php

namespace Example3\Unit;

use Example3\Unit\UnitInterface;

class Swordsman implements UnitInterface
{
    public function getAttackStrength(): int
    {
        return 6;
    }
}
