<?php

namespace Example3;

use Example3\UnitInterface;

interface RangedStrengthCalculatorInterface
{
    public function calculateStrength(UnitInterface $unit, int $howMany, int $weatherType): int;
}
