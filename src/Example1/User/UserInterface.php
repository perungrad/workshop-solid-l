<?php

namespace Example1\User;

use Example1\Town\TownInterface;

interface UserInterface
{
    public function setName(string $name): self;

    // spústa iných set/get metód pre usera, napr. setPassword, setEmail atď

    public function setCapitalTown(TownInterface $town): self;

    public function grantAccess(int $sectionId): bool;
}

