<?php

namespace Example1\User;

use Example1\Town\TownInterface;

class Player implements UserInterface
{
    private string $name;

    private TownInterface $capitalTown;

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setCapitalTown(TownInterface $town): self
    {
        $this->capitalTown = $town;

        return $this;
    }

    public function grantAccess(int $sectionId): bool
    {
        // Táto metóda je pre hráča zbytočná.
        // Ako by si ju naimplementoval/a?
    }
}

