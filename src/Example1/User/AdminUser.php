<?php

namespace Example1\User;

use Example1\Town\TownInterface;

class AdminUser implements UserInterface
{
    private string $name;

    /** @var array<int> */
    private array $allowedSections = [];

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setCapitalTown(TownInterface $town): self
    {
        // Táto metóda je pre administrátora zbytočná.
        // Ako by si ju naimplementoval/a?
    }

    public function grantAccess(int $sectionId): bool
    {
        $this->allowedSections[] = $sectionId;

        return true;
    }
}

