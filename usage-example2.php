<?php

use Example2\PremiumBuildingsProvider;
use Example2\BuildingsProvider;
use Example2\BuildingsProviderInterface;

require_once __DIR__ . '/vendor/autoload.php';

class UsageExample2
{
    public function run(BuildingsProviderInterface $buildingsProvider)
    {
        $buildings = $buildingsProvider->getBuildings();

        //echo "pocet budov: " . count($buildings) . "\n";

        foreach ($buildings as $building) {
            echo $building->getName() ."\n";
        }
    }
}

$example = new UsageExample2();

$example->run(new BuildingsProvider());
$example->run(new PremiumBuildingsProvider());
