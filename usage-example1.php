<?php

require_once __DIR__ . '/vendor/autoload.php';

use Example1\Town\Town;
use Example1\Town\TownInterface;
use Example1\User\Player;
use Example1\User\UserInterface;

class UsageExample1
{
    public function run(UserInterface $user, TownInterface $town)
    {
        $user->setName('fero');
        $user->setCapitalTown($town);
    }
}

$player = new Player();
$town = new Town();

$example = new UsageExample1();

$example->run($player, $town);
